const express = require('express');
const axios = require('axios');
const request = require('request');
const path = require('path');
const randomId = require('random-id');;
const cors = require('cors');
const iSHARE = require('./ishare.js')
const querystring = require('querystring');
const fs = require('fs');
const jwt = require('jsonwebtoken');

const port = process.env.SERVER_PORT || 8080;


const privateKey = fs.readFileSync('<location to private key.pem>', "utf8");
const publicKey = fs.readFileSync('<location to public key.pem>', "utf8");
const clientId = '<client EORI>'
const app = express(),
    bodyParser = require("body-parser")
app.use(express.static(path.join(__dirname, '../gui/dist')));
app.use(bodyParser.json());

// Add headers before the routes are defined
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.get("/health", async(req, res) => {
    res.status(200);
    res.send();
})

app.post('/sparql', async(req, res) => {

    try {

        const messageType = "ArtifactRequestMessage"
        const artifact = "http://sparql"

        //get capabilities to get the right URLs en the eori from the proxy

        const capabilitiesRequest = await axios.get(`${req.body.localproxy}/api/inner/capabilities`)
        const decoded_capabilitiesToken = jwt.decode(capabilitiesRequest.data.capabilities_token, { complete: true });
        const proxyId = decoded_capabilitiesToken.payload.capabilities_info.party_id
            //const versions = decoded_capabilitiesToken.payload.capabilities_info.supported_versions
        const public_features = decoded_capabilitiesToken.payload.capabilities_info.supported_versions[0].supported_features[0].public
        const tokenUrl = public_features.find(o => o.feature === 'access_token').url;

        //supported_features[0].public
        //const tokenUrl = public_features.get
        // const tokenUrl = features.[0].supported_features[0].public
        //create client assertion for consumer
        const clientAssertion = iSHARE.createClientAssertion(clientId, proxyId, privateKey, publicKey)

        //request token from proxy
        var reqBody = querystring.stringify({
            'grant_type': 'client_credentials',
            'scope': 'iSHARE',
            'client_assertion_type': 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
            'client_assertion': clientAssertion,
            'client_id': clientId,
        });
        var contentLength = reqBody.length;
        request.post({
            headers: {
                'Content-Length': contentLength,
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            url: tokenUrl,
            body: reqBody,
        }, async function(err, response, body) {

            try {
                //extract token from reply
                const access_token = JSON.parse(response.body).access_token;

                //use token to send request
                const bodyJson = {
                    messageType: messageType,
                    artifact: artifact,
                    provider: req.body.provider,
                    query: req.body.query,
                    format: req.body.format,
                    dataset: req.body.dataset,
                    remoteproxy: req.body.remoteproxy
                }
                const config = {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${access_token}`
                    }
                }
                const relayUrl = public_features.find(o => o.feature === 'relay').url;
                const data = await axios.post(relayUrl, bodyJson, config)
                res.send(data.data);
            } catch (error) {
                if (error.response) {
                    console.log(error)
                    res.status(error.response.status);
                    res.send(error.response.data);

                } else {
                    res.status(400);
                    res.send();
                    console.log(error)
                }
            }
        })
    } catch (error) {
        if (error.response) {
            console.log(error)
            res.status(error.response.status);
            res.send(error.response.data);

        } else {
            console.log(error)
            res.status(400);
            res.send();
        }
    }
});

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '../gui/dist/index.html'));
});

app.listen(port, () => {
    console.log(`Server listening on the port::${port}`);
});