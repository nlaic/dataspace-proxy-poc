const fs    = require('fs');
const jwt   = require('jsonwebtoken');

module.exports = {

    pemToX5C : (pemString, $Options) => {
        // Regex from https://github.com/digitalbazaar/forge lib/pem.js
        const pemRegex = /\s*-----BEGIN ([A-Z0-9- ]+)-----\r?\n?([\x21-\x7e\s]+?(?:\r?\n\r?\n))?([:A-Za-z0-9+\/=\s]+?)-----END \1-----/g;
        var x5c = []
        var match;
        while (true) {
            match = pemRegex.exec(pemString);
            if (!match) {
                break;
            }
            x5c.push(match[3].replace(/\s/g, ''));
        }
        return x5c;
    }
}
