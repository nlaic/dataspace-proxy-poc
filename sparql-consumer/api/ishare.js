const fs = require('fs');
const jwt = require('jsonwebtoken');
const axios = require('axios');
const { v4 } = require('uuid');
const util = require('util');
const certUtils = require('./certificateUtils.js')

function pemToX5C(pemString) {
    // Regex from https://github.com/digitalbazaar/forge lib/pem.js
    const pemRegex = /\s*-----BEGIN ([A-Z0-9- ]+)-----\r?\n?([\x21-\x7e\s]+?(?:\r?\n\r?\n))?([:A-Za-z0-9+\/=\s]+?)-----END \1-----/g;
    var x5c = []
    var match;
    while (true) {
        match = pemRegex.exec(pemString);
        if (!match) {
            break;
        }
        x5c.push(match[3].replace(/\s/g, ''));
    }
    return x5c;
}

module.exports = {
    createClientAssertion : (clientID, audience , privateKey, publicKey, $Options) => {
         return clientAssertionToken = jwt.sign({}, {
                key: privateKey,
                passphrase: 'tno123'
            }, {
                algorithm: 'RS256',
                expiresIn: 30,
                audience: audience,
                issuer: clientID,
                subject: clientID,
                jwtid: v4(),
                header: {
                    'x5c': pemToX5C(publicKey)
                }
            });
    }
}