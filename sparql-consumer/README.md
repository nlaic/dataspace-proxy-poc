# SAML SPARQL UI
This repository contains a Vue application that acts as the front-end for the Sparql query consumer.

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

## Project setup
```
cd api
npm install
cd gui
npm install
```

### Compiles and hot-reloads for development
```
cd api
node server.js
cd gui
npm run serve
```

### Docker Image usage

```
docker run -it --rm -p 8080:80 -e PROXY_ENDPOINT=<url to ishare proxy>  registry.ids.smart-connected.nl/ishare-sparql-ui
```


