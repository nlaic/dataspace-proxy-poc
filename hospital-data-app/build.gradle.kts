import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.text.SimpleDateFormat
import java.util.*

plugins {
	id("org.springframework.boot") version "2.4.2"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	id("com.google.cloud.tools.jib") version "3.0.0"
	kotlin("jvm") version "1.5.30"
	kotlin("plugin.spring") version "1.5.30"
	kotlin("plugin.serialization") version "1.5.30"
}

group = "nl.tno.ids"
version = "0.1.0"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
	jcenter()
	// Add the Fraunhover maven repository
	maven {
		url = uri("https://maven.iais.fraunhofer.de/artifactory/eis-ids-public/")
	}
	// Add the TNO Maven repositories
	maven {
		url = uri("https://ci.ids.smart-connected.nl/nexus/repository/tno-ids/")
		credentials {
			// Contact erwin.somers@tno.nl for TNO Maven repository credentials
		}
	}
	maven {
		url = uri("https://nexus.dataspac.es/repository/tsg-maven")
		credentials {
			// Contact erwin.somers@tno.nl for TNO Maven repository credentials
		}
	}
}

dependencies {
	// IDS dependencies
	implementation("nl.tno.ids", "base-data-app", "4.2.3-SNAPSHOT")
	implementation("commons-configuration", "commons-configuration", "1.10")

	implementation("org.apache.jena","jena-querybuilder","3.17.0")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
