package nl.tno.ids.nlaic.hospital.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "fuseki")
data class FusekiConfig(
    val endpoint: String
)
