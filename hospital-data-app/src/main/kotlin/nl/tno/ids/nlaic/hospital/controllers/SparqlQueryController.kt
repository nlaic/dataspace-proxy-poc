package nl.tno.ids.nlaic.hospital.controllers

import nl.tno.ids.nlaic.hospital.config.FusekiConfig
import nl.tno.ids.nlaic.hospital.handlers.SparqlQueryHandler
import nl.tno.ids.nlaic.hospital.model.Params
import org.apache.jena.query.QueryExecutionFactory
import org.apache.jena.query.QueryFactory
import org.apache.jena.query.ResultSetFormatter
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.io.ByteArrayOutputStream


@RestController
@RequestMapping(value = ["/api"])
class SparqlQueryController(private val fusekiConfig: FusekiConfig) {

    @PostMapping("/query")
    fun query(@RequestBody request: Params) : ResponseEntity<String> {

        logger.debug("received query: ${request.query}")
        val query = QueryFactory.create(request.query)

        return QueryExecutionFactory.sparqlService("${fusekiConfig.endpoint}/${request.dataset}" , query).use { qexec ->
            // Execute
            val rs = qexec.execSelect()
            var queryResponse = ""
            when (request.format) {
                "XML" -> {
                    queryResponse = ResultSetFormatter.asXMLString(rs)
                }
                "JSON" -> {
                    val outputStream = ByteArrayOutputStream()
                    ResultSetFormatter.outputAsJSON(outputStream, rs)
                    queryResponse = String(outputStream.toByteArray())
                }
            }
            logger.debug("result: $queryResponse")
            logger.debug("Resulting message size: {}", queryResponse.length)
            ResponseEntity.ok(queryResponse)
        }
    }

    companion object{
        private val logger = LoggerFactory.getLogger(SparqlQueryHandler::class.java)
    }
}
