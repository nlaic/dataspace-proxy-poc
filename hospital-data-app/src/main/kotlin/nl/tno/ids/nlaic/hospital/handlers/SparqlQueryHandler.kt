package nl.tno.ids.nlaic.hospital.handlers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import de.fraunhofer.iais.eis.*
import nl.tno.ids.base.StringMessageHandler
import nl.tno.ids.base.configuration.IdsConfig
import nl.tno.ids.common.multipart.MultiPart
import nl.tno.ids.common.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.DateUtil
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.nlaic.hospital.model.Params
import nl.tno.ids.nlaic.hospital.config.FusekiConfig
import org.apache.jena.query.QueryExecutionFactory
import org.apache.jena.query.QueryFactory
import org.apache.jena.query.ResultSetFormatter
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import java.io.ByteArrayOutputStream
import java.net.URI

@Component
class SparqlQueryHandler(private val idsConfig: IdsConfig,
                         private val fusekiConfig: FusekiConfig) : StringMessageHandler<ArtifactRequestMessage> {

    override fun handle(header: ArtifactRequestMessage, payload: String?): ResponseEntity<*> {

        val sparqlRequest: Params = mapper.readValue(payload, Params::class.java)

        logger.debug("received query: ${sparqlRequest.query}")
        val query = QueryFactory.create(sparqlRequest.query)
        logger.debug("executing query on  ${fusekiConfig.endpoint}/${sparqlRequest.dataset} -> ${sparqlRequest.query}")
        val queryResult = QueryExecutionFactory.sparqlService("${fusekiConfig.endpoint}/${sparqlRequest.dataset}" , query).use { qexec ->
            val rs = qexec.execSelect()
            when (sparqlRequest.format) {
                "XML" -> {
                    ResultSetFormatter.asXMLString(rs)
                }
                "JSON" -> {
                    val outputStream = ByteArrayOutputStream()
                    ResultSetFormatter.outputAsJSON(outputStream, rs)
                    String(outputStream.toByteArray())
                }
                else -> {
                    "Unsupported format"
                }
            }
        }

        logger.info("result: $queryResult")
        logger.info("Resulting message size: ${queryResult.length}")
        val multiPartMessage: MultiPartMessage = MultiPartMessage.Builder()
            .setHeader(
                ArtifactResponseMessageBuilder()
                    ._modelVersion_("4.2.3-SNAPSHOT")
                    ._issued_(DateUtil.now())
                    ._correlationMessage_(header.id)
                    ._issuerConnector_(URI.create(idsConfig.connectorId))
                    ._senderAgent_(URI.create(idsConfig.participantId))
                    ._recipientConnector_(header.issuerConnector)
                    ._recipientAgent_(header.senderAgent)
                    ._securityToken_(
                        DynamicAttributeTokenBuilder()
                        ._tokenFormat_(TokenFormat.JWT)
                        ._tokenValue_("test")
                        .build())
                    .build()
            )
            .setPayload(queryResult)
            .build()

        logger.debug("returning header: ${multiPartMessage.header.toJsonLD()} and payload : ${multiPartMessage.payload}")
        return MultiPart.toResponseEntity(multiPartMessage, HttpStatus.OK)
    }

    companion object{
        private val logger = LoggerFactory.getLogger(SparqlQueryHandler::class.java)
        private val mapper = ObjectMapper().registerModule(KotlinModule())
    }
}