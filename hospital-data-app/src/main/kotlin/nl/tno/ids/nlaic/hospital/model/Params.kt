package nl.tno.ids.nlaic.hospital.model

data class Params(
    val query: String,
    val format: String,
    val dataset: String
)
