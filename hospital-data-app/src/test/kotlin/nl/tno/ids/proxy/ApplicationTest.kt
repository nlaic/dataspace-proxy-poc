package nl.tno.ids.proxy

import org.junit.jupiter.api.Test
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.ComponentScan

@SpringBootTest(
    classes = [ApplicationTests.Application::class]
)

class ApplicationTests {

    @SpringBootApplication
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application

    @Test
    fun contextLoads() {
    }
}