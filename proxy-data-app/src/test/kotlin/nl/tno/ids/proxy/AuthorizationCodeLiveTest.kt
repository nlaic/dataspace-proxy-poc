package nl.tno.ids.proxy

import io.restassured.RestAssured
import io.restassured.response.Response
import nl.tno.ids.proxy.config.HarmonizationInConfig
import nl.tno.ids.proxy.config.HarmonizationOutConfig
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.test.context.TestPropertySource

//Before running this live test make sure both authorization server and resource server are running
@SpringBootTest(
    classes = [AuthorizationCodeLiveTest.Application::class]
)
@TestPropertySource(
    properties = ["spring.config.location = classpath:AuthorizationCodeLiveTest.yaml",
        "artifacts.location=./resources"]
)

@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class AuthorizationCodeLiveTest {


    @SpringBootApplication
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application

    @Autowired
    lateinit var harmonizationOutConfig: HarmonizationOutConfig
    @Autowired
    lateinit var harmonizationInConfig: HarmonizationInConfig

    @Disabled
    @Test
    @Order(0)
    fun givenUser_whenUseFooClient_thenOkForFooResourceOnly() {
        val accessToken = obtainAccessTokenWithAuthorizationCode(harmonizationOutConfig.username, harmonizationOutConfig.password)
        val resourceResponse: Response = RestAssured.given().header("Authorization", "Bearer $accessToken").get(
            RESOURCE_SERVER + "/api/proxy/resource/accesstest"
        )

        Assertions.assertEquals(200, resourceResponse.statusCode)
    }

    private fun obtainAccessTokenWithAuthorizationCode(username: String, password: String): String {
        val authorizeUrl = harmonizationOutConfig.authURI + "/auth"
        val tokenUrl = harmonizationOutConfig.authURI + "/token"
        val loginParams: MutableMap<String, String> = HashMap()
        loginParams["client_id"] = harmonizationInConfig.clientId
        loginParams["response_type"] = "code"
        loginParams["redirect_uri"] = REDIRECT_URL
        loginParams["scope"] = "read write"

        // user login
        var response: Response = RestAssured.given().formParams(loginParams).get(authorizeUrl)
        val cookieValue: String = response.getCookie("AUTH_SESSION_ID")
        val authUrlWithCode: String = response.htmlPath().getString("'**'.find{node -> node.name()=='form'}*.@action")

        // get code
        val codeParams: MutableMap<String, String> = HashMap()
        codeParams["username"] = username
        codeParams["password"] = password
        response = RestAssured.given().cookie("AUTH_SESSION_ID", cookieValue).formParams(codeParams)
            .post(authUrlWithCode)
        val location: String = response.getHeader(HttpHeaders.LOCATION)
        Assertions.assertEquals(HttpStatus.FOUND.value(), response.statusCode)
        val splitArray = location.split("#","=","&")
        val code = splitArray[3]

        //get access token
        val tokenParams: MutableMap<String, String> = HashMap()
        tokenParams["grant_type"] = "authorization_code"
        tokenParams["client_id"] = harmonizationInConfig.clientId
        tokenParams["client_secret"] = harmonizationInConfig.clientSecret
        tokenParams["redirect_uri"] = REDIRECT_URL
        tokenParams["code"] = code
        response = RestAssured.given().formParams(tokenParams)
            .post(tokenUrl)
        return response.jsonPath().getString("access_token")
    }

    companion object {

        const val RESOURCE_SERVER = "http://localhost:8080"
        private const val REDIRECT_URL = "http://localhost:8080/resource"
    }
}