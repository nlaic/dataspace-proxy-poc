package nl.tno.ids.proxy

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication


/**
 * IMPORTANT: the base package must be set to nl.tno.ids so that Spring boot searches that namespace for
 * dependencies to register.
 */

@SpringBootApplication(scanBasePackages = ["nl.tno.ids"])
@ConfigurationPropertiesScan("nl.tno.ids")
class Application

fun main(args: Array<String>) {
	runApplication<Application>(*args)
}
