package nl.tno.ids.proxy.inner


import io.restassured.RestAssured
import io.restassured.response.Response
import nl.tno.ids.proxy.RemoteRequest
import nl.tno.ids.proxy.config.HarmonizationOutConfig
import nl.tno.ids.proxy.outer.ProxyExternalSecurityController
import org.apache.http.NameValuePair
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.ContentType
import org.apache.http.entity.InputStreamEntity
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.message.BasicNameValuePair
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.io.File


@RestController
@RequestMapping(value = ["/api/proxy"])
class ProxyInternalController(private val httpClient: CloseableHttpClient,
                              private val harmonizationOutConfig: HarmonizationOutConfig) {

    @PostMapping("/relay")
    fun internal(@RequestBody payload: RemoteRequest,
                 @RequestHeader("destinationConnector") destinationConnector: String,
                 @RequestHeader("destinationProxy") destinationProxy: String) : ResponseEntity<*>
    {

        val response = sendMessageToProxy(destinationConnector,destinationProxy,payload)
        val contentType = response.second?.let { ContentType.create(response.second) ?: ContentType.create("application/sparql-query")}
        return ResponseEntity.status(response.first)
                             .header("Content-Type", contentType.toString())
                             .body(response.second.toString())
    }

    fun sendMessageToProxy(destinationConnector: String, destinationProxy: String, payload: RemoteRequest?): Triple<Int, String?, String> {

        //get token from the destination proxy
        val tokenRequest = HttpPost(destinationProxy + "/tokensimple")
        val tokenParameters: ArrayList<NameValuePair> = ArrayList<NameValuePair>()
        tokenParameters.add(BasicNameValuePair("username", harmonizationOutConfig.username))
        tokenParameters.add(BasicNameValuePair("password", harmonizationOutConfig.password))
        tokenRequest.entity = UrlEncodedFormEntity(tokenParameters, "UTF-8")

        logger.info("Requesting token at $destinationProxy")

        val accessToken = httpClient.execute(tokenRequest).use { response ->
            val statusCode = response.statusLine.statusCode
            if (statusCode !in 200..299) {
                logger.info("Received response with statuscode: {}", statusCode)
            }
            response.entity.content.toString()
        }

        if (accessToken == "")
        {
           return Triple(HttpStatus.INTERNAL_SERVER_ERROR.value(), "", "unable to obtain access token")
        }
        // set Forward-To header so the remote proxy knows where to send the request
        val dataRequest = HttpPost(destinationProxy + "/resource")
        dataRequest.addHeader("Forward-To", destinationConnector)
        dataRequest.addHeader("Authorization", "Bearer $accessToken")

        val file = File.createTempFile("proxy-data-app-" + System.currentTimeMillis(), ".http")
        if (payload != null) {
            file.writeText(payload.toString())
            dataRequest.entity = InputStreamEntity(file.inputStream())
        }

        try {
            return httpClient.execute(dataRequest).use { response ->
                val statusCode = response.statusLine.statusCode
                if (statusCode !in 200..299) {
                    logger.info("Received response with statuscode: {}", statusCode)
                }
                val contentTypeHeader = response.getFirstHeader("Content-Type")
                val contentTypeValue = contentTypeHeader?.value
                Triple(statusCode, contentTypeValue, response.entity.content.toString() )
            }
        } finally {
            file.delete()
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(ProxyInternalController::class.java)
    }
}