package nl.tno.ids.proxy.model

import com.fasterxml.jackson.annotation.JsonCreator
import java.net.URL
import java.util.*

data class Feature @JsonCreator constructor(
    val id: String,
    val feature: String,
    val description: String,
    val url: URL
)

data class Version @JsonCreator constructor(
    val version: String,
    val supported_features: Map<String, List<Feature>>
)

data class Capabilities @JsonCreator constructor(
    val party_id: String,
    val supported_versions: List<Version>
)

data class CapabilitiesPayload @JsonCreator constructor(
    val iss: String,
    val sub: String,
    val jti:String =  UUID.randomUUID().toString(),
    val iat: Long,
    val exp: Long = iat + 30,
    val capabilities_info : Capabilities
)

data class CapabilitiesHeader @JsonCreator constructor(
    val x5c: String
)