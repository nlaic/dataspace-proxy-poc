package nl.tno.ids.proxy.outer

import com.google.gson.Gson
import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.Util.asList
import nl.tno.ids.base.HttpHelper
import nl.tno.ids.base.configuration.IdsConfig
import nl.tno.ids.common.serialization.DateUtil
import nl.tno.ids.configuration.infomodel.defaults
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.proxy.RemoteRequest
import nl.tno.ids.proxy.config.HarmonizationInConfig
import org.apache.http.entity.ContentType
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI

@RestController
@RequestMapping(value = ["/api/proxy"])
class ProxyExternalController(private val idsConfig: IdsConfig,
                              //private val brokerClient: BrokerClient,
                              private val harmonizationInConfig: HarmonizationInConfig,
                              val httpHelper: HttpHelper
) {

    @PostMapping("/resource")
    fun resource(@RequestBody requestToForward: RemoteRequest): ResponseEntity<String> {

        logger.info("Received message from outside data space : $requestToForward")
        //var recipient: String= brokerClient.connectorToAccessUri(forwardTo)?.let { it }  ?: harmonizationInConfig.decentralURI

        val token = DynamicAttributeTokenBuilder()
            ._tokenFormat_(TokenFormat.JWT)
            ._tokenValue_("test")
            .build()

        val message = when(requestToForward.messageType) {

            "ArtifactRequestMessage" -> {
                logger.info("Creating ${requestToForward.messageType}, requested artifact: ${requestToForward.artifact}")
                ArtifactRequestMessageBuilder()
                    ._modelVersion_("4.2.3-SNAPSHOT")
                    ._issued_(DateUtil.now())
                    ._issuerConnector_(URI.create(idsConfig.connectorId))
                    ._senderAgent_(URI.create(idsConfig.participantId))
                    ._recipientConnector_(asList(URI.create(requestToForward.provider)))
                    ._recipientAgent_(asList(URI.create(requestToForward.provider)))
                    ._securityToken_(token)
                    ._requestedArtifact_(URI(requestToForward.artifact))
                    .build()
            }
            "QueryMessage" -> {
                logger.info("Creating ${requestToForward.messageType}")
                QueryMessageBuilder()
                    ._modelVersion_("4.2.3-SNAPSHOT")
                    ._issued_(DateUtil.now())
                    ._issuerConnector_(URI.create(idsConfig.connectorId))
                    ._senderAgent_(URI.create(idsConfig.participantId))
                    ._recipientScope_(QueryTarget.BROKER)
                    ._recipientConnector_(asList(URI.create(requestToForward.provider)))
                    ._recipientAgent_(asList(URI.create(requestToForward.provider)))
                    ._securityToken_(token)
                    .build()
                    .defaults()
            }
            "InvokeOperationMessage" -> {
                logger.info("Creating ${requestToForward.messageType}")
                InvokeOperationMessageBuilder()
                    ._modelVersion_("4.2.3-SNAPSHOT")
                    ._issued_(DateUtil.now())
                    ._issuerConnector_(URI.create(idsConfig.connectorId))
                    ._senderAgent_(URI.create(idsConfig.participantId))
                    ._recipientConnector_(asList(URI.create(requestToForward.provider)))
                    ._recipientAgent_(asList(URI.create(requestToForward.provider)))
                    ._securityToken_(token)
                    .build()
                    .defaults()
            }
            else -> {
                logger.info("Unsupported Messagetype:  ${requestToForward.messageType}")
                 return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Unsupported message type in requestMessage: $requestToForward ")
            }
        }
        logger.info("sending message : ${message.toJsonLD()} with body : ${ requestToForward.params} to : ${requestToForward.provider}")

        val response = httpHelper.toHTTP(requestToForward.provider, message, Gson().toJson(requestToForward.params), ContentType.APPLICATION_JSON)
        val payload = response.second.payload?.asString()
        logger.info("received ${response.second.payload?.asString()}")
        return ResponseEntity.status(response.first).body(payload)
    }

    @GetMapping("/resource/accesstest")
    fun resource(): ResponseEntity<String> {
        return ResponseEntity.status(200).contentType(MediaType.TEXT_PLAIN).body("success")
    }

    companion object{
        private val logger = LoggerFactory.getLogger(ProxyExternalController::class.java)
    }
}