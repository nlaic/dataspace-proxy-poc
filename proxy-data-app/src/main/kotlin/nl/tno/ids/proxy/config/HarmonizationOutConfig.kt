package nl.tno.ids.proxy.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "harmonization-out")
data class HarmonizationOutConfig (

    val username: String,
    val password: String,
    val authURI: String

)