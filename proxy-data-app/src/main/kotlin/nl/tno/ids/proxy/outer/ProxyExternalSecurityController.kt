package nl.tno.ids.proxy.outer

import io.restassured.RestAssured
import io.restassured.response.Response
import nl.tno.ids.proxy.config.HarmonizationInConfig
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.TimeUnit

@RestController
@RequestMapping(value = ["/api/security"])
class ProxyExternalSecurityController(private val harmonizationInConfig: HarmonizationInConfig) {

    @PostMapping("/authorization")
    fun authorization(@RequestParam("response_type") response_type: String,
                 @RequestParam("client_id") client_id: String,
                 @RequestParam("client_assertion_type") client_assertion_type: String,
                 @RequestParam("client_assertion") client_assertion: String): ResponseEntity<String> {

        logger.debug(
            "Received a authorization request : \nclient_id: {} \n client_assertion: {}",
            client_id,
            client_assertion
        )
        //response_type should be code
        when {
            ("code" != response_type) -> {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("response_type should always be \"code\", received $response_type instead")
            }
            (client_id.startsWith("EU.EORI.")) -> {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("client_id should always be a EORI and start with \"EU.EORI.\", received $client_id instead")
            }
            ("urn:ietf:params:oauth:client-assertion.Type:jwt-bearer" != client_assertion_type) -> {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("client_assertion_type should always be\"urn:ietf:params:oauth:client-assertion.Type:jwt-bearer\", received $client_assertion_type instead")
            }
            else -> {
                // check client assertion and
                return ResponseEntity.status(HttpStatus.OK)
                    .body("Not implemented. Outside of the scope of this poc")
            }
        }
    }

    @PostMapping("/token/jwt")
    fun getToken(@RequestParam("grant_type") grant_type: String,
                 @RequestParam("code") code: String,
                 @RequestParam("redirect_uri") redirect_uri: String,
                 @RequestParam("client_id") client_id: String,
                 @RequestParam("client_assertion_type") client_assertion_type: String,
                 @RequestParam("client_assertion") client_assertion: String): ResponseEntity<String> {

        logger.debug(
            "Received a token request : \ngrant_type: $grant_type \n code: $code",
            client_id,
            client_assertion
        )
        //response_type should be code
        when {
            ("authorization code”" != grant_type) ->
            {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("grant_type should always be \"authorization code”\", received $grant_type instead")
            }
            (client_id.startsWith("EU.EORI.")) ->
            {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("client_id should always be a EORI and start with \"EU.EORI.\", received $client_id instead")
            }
            ("urn:ietf:params:oauth:client-assertion.Type:jwt-bearer" != client_assertion_type) ->
            {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("client_assertion_type should always be\"urn:ietf:params:oauth:client-assertion.Type:jwt-bearer\", received $client_assertion_type instead")
            }
            else ->
            {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("Not implemented here, to request a token use keycloak:  ")
            }
        }
    }

    @PostMapping("/token/userpass")
    fun getTokenSimple(@RequestParam("username") username: String?,
                       @RequestParam("password") password: String?) : ResponseEntity<String> {

        logger.debug(
            "Received a token request"
        )

        //response_type should be code
        when {
            (username != null && password != null)  ->
            {
                val accessToken = obtainAccessTokenWithAuthorizationCode(username, password)
                return ResponseEntity.status(HttpStatus.OK)
                    .body(accessToken)
            }
            else ->
            {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("username and password mandatory")
            }
        }
    }

    private fun obtainAccessTokenWithAuthorizationCode(username: String, password: String): String? {

        val authorizeUrl = harmonizationInConfig.authURI + "/auth"
        val tokenUrl = harmonizationInConfig.authURI + "/token"
        val loginParams: MutableMap<String, String> = HashMap()
        val redirectURI = harmonizationInConfig.redirectURI
        val scope = harmonizationInConfig.scope

        loginParams["client_id"] = harmonizationInConfig.clientId
        loginParams["response_type"] = "code"
        loginParams["redirect_uri"] = redirectURI
        loginParams["scope"] = scope

        logger.info("Getting sessionID from $authorizeUrl , with : ${loginParams}")
        // user login
        var response: Response = RestAssured.given().formParams(loginParams).relaxedHTTPSValidation().get(authorizeUrl)
        val cookieValue = response.cookie("AUTH_SESSION_ID")
            //?.let {response.getCookie("AUTH_SESSION_ID") } ?: response.getCookie("AUTH_SESSION_ID_LEGACY")
        val authUrlWithCode: String = response.htmlPath().getString("'**'.find{node -> node.name()=='form'}*.@action")

        // get code
        val codeParams: MutableMap<String, String> = HashMap()
        codeParams["username"] = username
        codeParams["password"] = password

        logger.info("Getting location from $authUrlWithCode , with : ${codeParams}")
        response = RestAssured.given()
                              .cookie("AUTH_SESSION_ID", cookieValue).formParams(codeParams)
                                .relaxedHTTPSValidation().post(authUrlWithCode)
        val location: String = response.getHeader(HttpHeaders.LOCATION)

        when {
            (HttpStatus.FOUND.value() != response.statusCode) -> {
                return null
            }
            else -> {
                val splitArray = location.split("#","=","&")
                val code = splitArray[3]

                //get access token
                val tokenParams: MutableMap<String, String> = HashMap()
                tokenParams["grant_type"] = "authorization_code"
                tokenParams["client_id"] = harmonizationInConfig.clientId
                tokenParams["client_secret"] = harmonizationInConfig.clientSecret
                tokenParams["redirect_uri"] = redirectURI
                tokenParams["code"] = code

                logger.info("Getting token from $tokenUrl , with : $tokenParams")
                response = RestAssured.given().formParams(tokenParams).relaxedHTTPSValidation().post(tokenUrl)
                logger.info("Received token from $tokenUrl: ${response.body.asString()}")
                val accessToken = response.jsonPath().getString("access_token")
                logger.info("got token: $accessToken")
                return accessToken
            }
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(ProxyExternalSecurityController::class.java)

    }
}