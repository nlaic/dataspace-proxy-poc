package nl.tno.ids.proxy.inner
 

import nl.tno.ids.base.StringMessageHandler
import nl.tno.ids.base.multipart.MultiPartMessage
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.proxy.config.HarmonizationOutConfig
import nl.tno.ids.proxy.model.ProxyRequest
import org.apache.http.NameValuePair
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.InputStreamEntity
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.message.BasicNameValuePair
import org.springframework.http.ResponseEntity
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import java.io.File


// Inherit from MessageHandler to register message handlers of a specific IDS message.
// Spring will register the Message Handler using Dependency Injection
@Component
class ProxyMessageHandler(private val httpClient: CloseableHttpClient,
                          private val harmonizationOutConfig: HarmonizationOutConfig
) : StringMessageHandler<ProxyRequest> {

  private val logger = LoggerFactory.getLogger(ProxyMessageHandler::class.java)

  override fun handle(header: ProxyRequest, payload: String?): ResponseEntity<*> {

    logger.info("Received message from inside data space : {}, {}", header.toJsonLD(), payload)
    logger.info(
      "Message needs to be forwarded to proxy: {}, with: {} as final destination",
      header.destinationProxy,
      header.destinationConnector
    )

    // send the message to the receiving data space proxy
    val response = sendMessageToProxy(header, payload)
    // create IDS response message and send it back to origin in incoming message
    return ResponseEntity.status(response.first).body(response.second.toString())

  }

  fun sendMessageToProxy(header: ProxyRequest, body: String?): Pair<Int,MultiPartMessage> {

    logger.info("Forwarding Synchronous: {} via: {}, content: {}", header.destinationProxy.toString(), header.destinationConnector.toString(), body)

    val destinationProxy = header.destinationProxy.toString()
    val tokenRequest = HttpPost("$destinationProxy/tokensimple")
    val tokenParameters: ArrayList<NameValuePair> = ArrayList<NameValuePair>()
    tokenParameters.add(BasicNameValuePair("username", harmonizationOutConfig.username))
    tokenParameters.add(BasicNameValuePair("password", harmonizationOutConfig.password))
    tokenRequest.entity = UrlEncodedFormEntity(tokenParameters, "UTF-8")

    logger.info("Requesting token at $destinationProxy")
    val accessToken = httpClient.execute(tokenRequest).use { response ->
      val statusCode = response.statusLine.statusCode
      if (statusCode !in 200..299) {
        logger.info("Received response with statuscode: {}", statusCode)
      }
      response.entity.content.toString()
    }

    val regex = """^[A-Za-z0-9-_=]+\\.[A-Za-z0-9-_=]+\\.?[A-Za-z0-9-_.+/=]*\$""".toRegex()
    if (regex.matches((accessToken)))
    {
      val responseMultiPartMessage = MultiPartMessage(header,"Error requesting accessToken")
      return Pair(HttpStatus.INTERNAL_SERVER_ERROR.value(),responseMultiPartMessage)
    }
    val httpPost = HttpPost(header.destinationProxy)

    // set Forward-To header so the t=remote proxy knows where to send the request
    httpPost.addHeader("Forward-To", header.destinationConnector.toString())
    httpPost.addHeader("Authorization", "Bearer $accessToken")

    val file = File.createTempFile("proxy-data-app-" + System.currentTimeMillis(), ".http")

    if (body != null) {
      file.writeText(body)
      httpPost.entity = InputStreamEntity(file.inputStream())
    }

    try {
      return httpClient.execute(httpPost).use { response ->
        val statusCode = response.statusLine.statusCode
        if (statusCode !in 200..299) {
          logger.info("Received response with statuscode: {}", statusCode)
        }
        val contentTypeHeader = response.getFirstHeader("Content-Type")
        val contentTypeValue = contentTypeHeader?.value
        val responseMultiPartMessage = MultiPartMessage.parse(response.entity.content, contentTypeValue)
        Pair(statusCode, responseMultiPartMessage)
      }
    } finally {
      file.delete()
    }
  }

}
