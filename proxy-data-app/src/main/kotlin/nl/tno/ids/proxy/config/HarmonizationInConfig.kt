package nl.tno.ids.proxy.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "harmonization-in")
data class HarmonizationInConfig (

    val clientId: String,
    val clientSecret: String,
    val authURI: String,
    val redirectURI: String,
    val baseUrl: String,
    val scope: String,
    val pk12: String
)