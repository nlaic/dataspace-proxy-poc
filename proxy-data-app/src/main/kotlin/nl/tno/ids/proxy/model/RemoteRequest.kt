package nl.tno.ids.proxy

import com.fasterxml.jackson.annotation.JsonCreator

enum class Actions(val action: String) {
    add("add"), delete("delete"), modify("modify"), read("read")
}

enum class Method(val method: String) {
    POST("POST"), PUT("PUT"), DELETE("DELETE"), GET("GET")
}


data class RemoteRequest @JsonCreator constructor(
    val action: Actions = Actions.read,
    val method: Method = Method.GET,
    val params: Map<String,String>?,
    val messageType: String,
    val artifact: String?,
    val provider: String

)
