package nl.tno.ids.proxy.outer


import com.nimbusds.jose.JWSAlgorithm
import com.nimbusds.jose.JWSHeader
import com.nimbusds.jose.JWSSigner
import com.nimbusds.jose.crypto.RSASSASigner
import com.nimbusds.jose.util.Base64
import com.nimbusds.jose.util.Base64URL
import com.nimbusds.jwt.JWTClaimsSet
import com.nimbusds.jwt.SignedJWT
import nl.tno.ids.base.HttpHelper
import nl.tno.ids.base.configuration.IdsConfig
import nl.tno.ids.proxy.config.HarmonizationInConfig
import nl.tno.ids.proxy.model.*
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.io.FileInputStream
import java.net.URL
import java.security.KeyStore
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.util.UUID
import java.util.Date


@RestController
@RequestMapping(value = ["/api/proxy"])
class ProxyExternalCapabilitiesController(private val harmonizationInConfig: HarmonizationInConfig,
) {

    private fun loadKeyStore()  {
        keyStore.load(FileInputStream("keystore.jks"), harmonizationInConfig.clientId.toCharArray())
    }

    private fun getPrivateKey() : RSAPrivateKey
    {
        if (!keyStore.containsAlias(harmonizationInConfig.clientId))
        {
            loadKeyStore()
        }
        return keyStore.getKey(harmonizationInConfig.clientId, harmonizationInConfig.clientId.toCharArray()) as RSAPrivateKey
    }

    private fun getPublicKey() :  RSAPublicKey {
        loadKeyStore()
        if (!keyStore.containsAlias(harmonizationInConfig.clientId))
        {

        }
        return keyStore.getCertificate(harmonizationInConfig.clientId) as RSAPublicKey
    }

    @GetMapping("/capabilities")
    fun capabilities(): ResponseEntity<String> {

        val features: MutableList<Feature> = mutableListOf()
        features.add( Feature(
            UUID.randomUUID().toString(),
            "access_token_user",
            "Aquire an access token (based on user/pass)",
            URL(harmonizationInConfig.baseUrl.toString() + "/api/security/token/userpass")))
        features.add(Feature(UUID.randomUUID().toString(),
            "access_token_jwt",
            "Aquire an access token (based on jwt)",
            URL(harmonizationInConfig.baseUrl.toString() + "/api/security/token//jwt")))
        features.add(Feature(UUID.randomUUID().toString(),
            "resource",
            "request a resource from a participant in the data space",
            URL(harmonizationInConfig.baseUrl.toString() + "/api/proxy/resource")))

        val suppFeat: MutableMap<String, List<Feature>> = mutableMapOf()
        suppFeat["public"] = features

        val suppVersions: MutableList<Version> = mutableListOf()
        suppVersions.add(Version("1.0",suppFeat))

        val capabilities: Capabilities = Capabilities(harmonizationInConfig.clientId, suppVersions)

        val pubKey: RSAPublicKey =  getPublicKey()
        val privKey : RSAPrivateKey = getPrivateKey()

        val iat = Date().time

        val claimsSet = JWTClaimsSet.Builder()
            .subject(harmonizationInConfig.clientId)
            .issueTime(Date(iat))
            .expirationTime(Date(iat+30))
            .issuer(harmonizationInConfig.clientId)
            .claim("jti", UUID.randomUUID().toString())
            .claim("capabilities", capabilities )
            .build()


        val certChain: MutableList<Base64> =  mutableListOf()
        certChain.add(Base64.encode(pubKey.encoded))
        val header = JWSHeader.Builder(JWSAlgorithm.RS256)
            .x509CertChain(certChain)
            .build()

        val signedJWT = SignedJWT(header, claimsSet)
        //val sigInput = Base64URL.encode(signedJWT.signingInput)
        val signer: JWSSigner = RSASSASigner(privKey)
        signedJWT.sign(signer)
        return ResponseEntity.status(200).body(signedJWT.toString())

    }

    companion object{
        private val logger = LoggerFactory.getLogger(ProxyExternalCapabilitiesController::class.java)
        val keyStore: KeyStore = KeyStore.getInstance("JKS")


    }
}