package nl.tno.ids.proxy.model;

import java.net.URI;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import de.fraunhofer.iais.eis.Builder;
import de.fraunhofer.iais.eis.DynamicAttributeToken;
import de.fraunhofer.iais.eis.Token;
import de.fraunhofer.iais.eis.util.*;

public class ProxyRequestBuilder implements Builder<ProxyRequest> {

    private ProxyRequestImpl proxyForwardMessageImpl;

    public ProxyRequestBuilder() {
        proxyForwardMessageImpl = new ProxyRequestImpl();
    }

    public ProxyRequestBuilder(URI id) {
        this();
        proxyForwardMessageImpl.id = id;
    }

    /**
     * This function allows setting a value for _destinationProxy
     *
     * @param _destinationProxy_ desired value to be set
     * @return Builder object with new value for _requestedArtifact
     */
    public ProxyRequestBuilder _destinationProxy_(URI _destinationProxy_) {
        this.proxyForwardMessageImpl.setDestinationProxy(_destinationProxy_);
        return this;
    }

    /**
     * This function allows setting a value for _destinationConnector
     *
     * @param _destinationConnector_ desired value to be set
     * @return Builder object with new value for _requestedArtifact
     */
    public ProxyRequestBuilder _destinationConnector_(URI _destinationConnector_) {
        this.proxyForwardMessageImpl.setDestinationConnector(_destinationConnector_);
        return this;
    }

    /**
     * This function allows setting a value for _modelVersion
     *
     * @param _modelVersion_ desired value to be set
     * @return Builder object with new value for _modelVersion
     */
    public ProxyRequestBuilder _modelVersion_(String _modelVersion_) {
        this.proxyForwardMessageImpl.setModelVersion(_modelVersion_);
        return this;
    }

    /**
     * This function allows setting a value for _issued
     *
     * @param _issued_ desired value to be set
     * @return Builder object with new value for _issued
     */
    public ProxyRequestBuilder _issued_(XMLGregorianCalendar _issued_) {
        this.proxyForwardMessageImpl.setIssued(_issued_);
        return this;
    }

    /**
     * This function allows setting a value for _correlationMessage
     *
     * @param _correlationMessage_ desired value to be set
     * @return Builder object with new value for _correlationMessage
     */
    public ProxyRequestBuilder _correlationMessage_(URI _correlationMessage_) {
        this.proxyForwardMessageImpl.setCorrelationMessage(_correlationMessage_);
        return this;
    }

    /**
     * This function allows setting a value for _issuerConnector
     *
     * @param _issuerConnector_ desired value to be set
     * @return Builder object with new value for _issuerConnector
     */
    public ProxyRequestBuilder _issuerConnector_(URI _issuerConnector_) {
        this.proxyForwardMessageImpl.setIssuerConnector(_issuerConnector_);
        return this;
    }

    /**
     * This function allows setting a value for _recipientConnector
     *
     * @param _recipientConnector_ desired value to be set
     * @return Builder object with new value for _recipientConnector
     */
    public ProxyRequestBuilder _recipientConnector_(List<URI> _recipientConnector_) {
        this.proxyForwardMessageImpl.setRecipientConnector(_recipientConnector_);
        return this;
    }

    /**
     * This function allows adding a value to the List _recipientConnector
     *
     * @param _recipientConnector_ desired value to be added
     * @return Builder object with new value for _recipientConnector
     */
    public ProxyRequestBuilder _recipientConnector_(URI _recipientConnector_) {
        this.proxyForwardMessageImpl.getRecipientConnector().add(_recipientConnector_);
        return this;
    }

    /**
     * This function allows setting a value for _senderAgent
     *
     * @param _senderAgent_ desired value to be set
     * @return Builder object with new value for _senderAgent
     */
    public ProxyRequestBuilder _senderAgent_(URI _senderAgent_) {
        this.proxyForwardMessageImpl.setSenderAgent(_senderAgent_);
        return this;
    }

    /**
     * This function allows setting a value for _recipientAgent
     *
     * @param _recipientAgent_ desired value to be set
     * @return Builder object with new value for _recipientAgent
     */
    public ProxyRequestBuilder _recipientAgent_(List<URI> _recipientAgent_) {
        this.proxyForwardMessageImpl.setRecipientAgent(_recipientAgent_);
        return this;
    }

    /**
     * This function allows adding a value to the List _recipientAgent
     *
     * @param _recipientAgent_ desired value to be added
     * @return Builder object with new value for _recipientAgent
     */
    public ProxyRequestBuilder _recipientAgent_(URI _recipientAgent_) {
        this.proxyForwardMessageImpl.getRecipientAgent().add(_recipientAgent_);
        return this;
    }

    /**
     * This function allows setting a value for _securityToken
     *
     * @param _securityToken_ desired value to be set
     * @return Builder object with new value for _securityToken
     */
    public ProxyRequestBuilder _securityToken_(DynamicAttributeToken _securityToken_) {
        this.proxyForwardMessageImpl.setSecurityToken(_securityToken_);
        return this;
    }

    /**
     * This function allows setting a value for _authorizationToken
     *
     * @param _authorizationToken_ desired value to be set
     * @return Builder object with new value for _authorizationToken
     */
    public ProxyRequestBuilder _authorizationToken_(Token _authorizationToken_) {
        this.proxyForwardMessageImpl.setAuthorizationToken(_authorizationToken_);
        return this;
    }

    /**
     * This function allows setting a value for _transferContract
     *
     * @param _transferContract_ desired value to be set
     * @return Builder object with new value for _transferContract
     */
    public ProxyRequestBuilder _transferContract_(URI _transferContract_) {
        this.proxyForwardMessageImpl.setTransferContract(_transferContract_);
        return this;
    }

    /**
     * This function allows setting a value for _contentVersion
     *
     * @param _contentVersion_ desired value to be set
     * @return Builder object with new value for _contentVersion
     */
    public ProxyRequestBuilder _contentVersion_(String _contentVersion_) {
        this.proxyForwardMessageImpl.setContentVersion(_contentVersion_);
        return this;
    }

    /**
     * This function takes the values that were set previously via the other functions of this class and
     * turns them into a Java bean.
     *
     * @return Bean with specified values
     * @throws ConstraintViolationException This exception is thrown, if a validator is used and a
     *         violation is found.
     */
    @Override
    public ProxyRequest build() throws ConstraintViolationException {
        VocabUtil.getInstance().validate(proxyForwardMessageImpl);
        return proxyForwardMessageImpl;
    }
}

