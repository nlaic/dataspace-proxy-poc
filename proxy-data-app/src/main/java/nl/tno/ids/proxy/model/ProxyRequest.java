package nl.tno.ids.proxy.model;

import java.net.URI;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import de.fraunhofer.iais.eis.RequestMessage;
import de.fraunhofer.iais.eis.util.*;

/**
 * Message asking for retrieving the specified Artifact as the payload of an ArtifactResponse
 * message.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "@type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ProxyRequestImpl.class)
})
public interface ProxyRequest extends RequestMessage {

    // standard methods

    @Beta
    ProxyRequest deepCopy();

    // accessor methods as derived from the IDS Information Model ontology

    /**
     * References the Data Space Proxy in the context of a request
     *
     * @return Returns the URI for the property _destinationProxy.
     */
    @NotNull
    @JsonProperty("ids:destinationProxy")
    URI getDestinationProxy();

    /**
     * References the Data Space Proxy in the context of a request.
     *
     * @param _destinationProxy_ desired value for the property _destinationProxy.
     */
    void setDestinationProxy(URI _destinationProxy_);

    /**
     * References the remote connector in the context of a request.
     *
     * @return Returns the URI for the property _destinationProxyConnector.
     */
    @NotNull
    @JsonProperty("ids:destinationProxyConnector")
    URI getDestinationConnector();

    /**
     * References the remote connector in the context of a request.
     *
     * @param _destinationConnector_ desired value for the property _destinationConnector.
     */
    void setDestinationConnector(URI _destinationConnector_);
}
