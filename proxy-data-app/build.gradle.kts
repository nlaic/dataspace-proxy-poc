import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.text.SimpleDateFormat
import java.util.*

plugins {
	id("org.springframework.boot") version "2.4.2"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	id("com.google.cloud.tools.jib") version "3.0.0"
	kotlin("jvm") version "1.5.30"
	kotlin("plugin.spring") version "1.5.30"
	kotlin("plugin.serialization") version "1.5.30"
}

group = "nl.tno.ids"
version = "0.1.0"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
	// Add the Fraunhover maven repository
	maven {
		url = uri("https://maven.iais.fraunhofer.de/artifactory/eis-ids-public/")
	}
	// Add the TNO Maven repository
	maven {
		url = uri("https://ci.ids.smart-connected.nl/nexus/repository/tno-ids/")
		credentials {
			// Contact erwin.somers@tno.nl for TNO Maven repository credentials
		}
	}
	maven {
		url = uri("https://nexus.dataspac.es/repository/tsg-maven")
	}
	maven {
		url = uri("https://nexus.dataspac.es/repository/tsg-maven-private")
		credentials {
			// Contact erwin.somers@tno.nl for TNO Maven repository credentials
		}
	}
}

//needed for restassured > 4.2.x
extra.apply {
	set("groovy.version", "3.0.9")
}


dependencies {
	// IDS dependencies
	implementation("nl.tno.ids", "base-data-app", "4.2.3-SNAPSHOT")
	implementation("commons-configuration", "commons-configuration", "1.10")
	implementation("org.springframework.boot","spring-boot-starter-security","2.6.2")
	implementation("org.springframework.boot","spring-boot-starter-web")
	implementation("org.springframework.boot","spring-boot-starter-oauth2-resource-server","2.6.2")
	implementation("io.rest-assured","rest-assured","4.5.0")
	implementation("io.rest-assured","json-path","4.5.0")
	implementation("io.rest-assured","xml-path","4.5.0")
	implementation("io.rest-assured","kotlin-extensions","4.5.0")
	implementation("com.google.code.gson","gson","2.8.9")
	// JWT lib
	implementation("com.nimbusds","nimbus-jose-jwt","9.20")

	testImplementation("org.springframework.boot","spring-boot-starter-test","2.4.5")
	testImplementation("org.apache.camel","camel-test-spring-junit5","3.14.0")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
