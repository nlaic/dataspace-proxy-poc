# Dataspace Proxy PoC

In this repository the sources and scripts needed to run the Dataspace Proxy PoC will be placed soon.

## The PoC
This PoC demonstrates interoperability between an iSHARE based dataspace and a IDS based dataspace, using a simple proxy architecture (as proposed by the [Data Sharing Coalition](https://datasharingcoalition.eu/ "This link takes you to the website of the DSC") in the [Data Sharing Canvas](https://datasharingcoalition.eu/app/uploads/2021/04/data-sharing-canvas-30-04-2021.pdf)).

Using the dataspace proxies the iSHARE beased request is transported to the IDS based data provider and translated into a harmonized message by the iSHARE proxy and then into an IDS
message by the IDS proxy.

This PoC uses KeyCloack as an intermediary IAA service in the  harmonisation space. This enabled the establishment of an trust chain between the iSHARE beased consumer and the IDS based provider. To ensure trust, the idenity of the node in the chain is verified by the  responsible authority in the space the message is transported through.

## Project structure

The PoC consists of several microservices (further explanation [here](#technical-components)).
Some of those services are located as subfolders in this project:

- [Helm Chart](./deployment/README.md) contains the Helm code used for deploying the services to a Kubernetes cluster.
- [Hospital Data App](./hospital-data-app/README.md) contains the code for the providing data app. 
- [IDS Proxy Data App](./proxy-data-app/README.md) contains the code for the proxy data app
- [iSHARE proxy](./ishare-proxy/README.md) contains the code for the iSHARE data app. the proxy implements the iSHARE authentication process. 
- [SPARQL Consumer](./sparql-consumer/README.md) contains the javascript code for the consuming app, which sends, sparql request to a hospital-data-app via the proxies.

## Technical Components

The PoC is based on the [IDS reference architecture](https://www.internationaldataspaces.org/wp-content/uploads/2019/03/IDS-Reference-Architecture-Model-3.0.pdf), the Data Sharing Coaltion [UCIG](https://datasharingcoalition.eu/nl/onze-aanpak-en-tools/use-case-implementation-guide/) and the [iSHARE authentication proces](http://dev.ishareworks.org/m2m/authentication.html)

- Sparql consumer: UI ([VueJS](https://vuejs.org/)) and a [NodeJS](https://nodejs.org/en/about/) javascript backend to send SPARQL requests to the hospital, via the iSHARE - and IDS proxy, and displays the response
- Hospital:  hospital providing sparql querying service on an ADNI dataset using IDS.
- iSHARE proxy: iSHARE based service provider, receiving messages from within the iSHARE dataspace and relaying this message to the IDS proxy data app.
- IDS proxy: An IDS based proxy service, which translates the incoming proxy request to an IDS based message and relays it to the destination connector.
- KeyCloak: An IAA service provider used by the proxies to identify and authenticate each other in the harmonisation space. 
- 
## Architecture

![overview](images/overview.png "overview")