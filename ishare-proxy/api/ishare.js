const jwt = require('jsonwebtoken');
const { v4 } = require('uuid');
const forge = require('node-forge');
const fs = require('fs')
const logger = require('loglevel').getLogger("ishare");
const logLevel = process.env.LOG_LEVEL || "debug"
logger.setLevel(logLevel)

const CA_Folder = process.env.CA_FOLDER || './cert/CA'

const caStore = createCAStore(CA_Folder)

function createCAStore(CA_path) {
    var store = forge.pki.createCaStore();
    fs.readdir(CA_path, function(err, filenames) {
        if (err) {
            logger.debug(`Unable to read files from $CA_Folder`);
            return;
        }
        filenames.forEach(function(filename) {
            const pem = fs.readFileSync(`${CA_Folder}/${filename}`, 'utf8')
            store.addCertificate(pem)
        })
    })
    return store
}

function pemToX5C(pemString) {
    // Regex from https://github.com/digitalbazaar/forge lib/pem.js
    const pemRegex = /\s*-----BEGIN ([A-Z0-9- ]+)-----\r?\n?([\x21-\x7e\s]+?(?:\r?\n\r?\n))?([:A-Za-z0-9+\/=\s]+?)-----END \1-----/g;
    var x5c = []
    var match;
    while (true) {
        match = pemRegex.exec(pemString);
        if (!match) {
            break;
        }
        x5c.push(match[3].replace(/\s/g, ''));
    }
    return x5c;
}

const clientAccessToken = (sp_id, client_id, privateKey) => {
    var iat = Math.floor(new Date() / 1000)
    var payload = {
        "iss": sp_id,
        "aud": sp_id,
        "client_id": client_id,
        "exp": iat + 3600,
        "nbf": iat,
        "scope": ["iSHARE"]
    };
    return jwt.sign(payload, privateKey, { algorithm: 'RS256', noTimestamp: true })
}

const serviceAssertionToken = (privateKey, publicKey, audience, issuer) => {

    return jwt.sign({}, privateKey, {
        algorithm: 'RS256',
        expiresIn: 30,
        audience: audience,
        issuer: issuer,
        subject: issuer,
        jwtid: v4(),
        header: {
            'x5c': pemToX5C(publicKey)
        }
    });
}

const verifyClientAccessToken = (accessToken, privateKey, eori_sp) => {
    logger.debug(`Access Token: ${accessToken}`);
    var epoch = Math.floor(new Date() / 1000)
    try {
        var decoded_token = jwt.verify(accessToken, privateKey, {
            algorithms: 'RS256'
        });
    } catch (err) {
        logger.info('signature not ours');
        console.debug(err);
        return false
    }
    logger.debug(`decoded token: $decoded_token`);
    if (decoded_token.iss != eori_sp) {
        logger.info('token not ours');
        return false
    } else {
        return true
    }
}


const check_assertion = (body, eori_sp) => {
    var epoch = Math.floor(new Date() / 1000)
    var ca = body.client_assertion
    client_id = body.client_id
    logger.debug(body.client_id)
    logger.debug(`cient assertion to check : ${ca}`)
    let description = '';
    if (!body.client_assertion) {
        description = description.concat('=>', 'Request has no client_assertion. ')
    }
    const client_assertion = ca.replace(/\s/g, '')
    if (!body.grant_type) {
        description = description.concat('=>', 'Request has no grant type. ')
    }
    if (body.grant_type != 'client_credentials') {
        description = description.concat('=>', 'Request has other value than client_credentials. ')
    }
    if (!body.scope) {
        description = description.concat('=>', 'Request has no scope. ')
    }
    if (body.scope != 'iSHARE') {
        description = description.concat('=>', 'Request has other value than iSHARE. ')
    }
    if (!body.client_assertion_type) {
        description = description.concat('=>', 'Request scope has no client_assertion_type. ')
    }
    if (body.client_assertion_type != 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer') {
        description = description.concat('=>', 'Request has other value than urn:ietf:params:oauth:client-assertion-type:jwt-bearer. ')
    }
    if (!body.client_id) {
        description = description.concat('=>', 'Request has no client_id. ')
    }
    if (description.length > 0) {
        return { validated: false, description }
    }
    var decodedClientAssertion = jwt.decode(client_assertion, {
        complete: true
    });

    try {
        var x5c = decodedClientAssertion.header.x5c
    } catch (e) {
        description = 'Request has no client_id. '
        return { validated: false, description }
    }
    var alg = decodedClientAssertion.header.alg
    var typ = decodedClientAssertion.header.typ
    if (!alg) {
        description = 'Client assertion JWT header ' + 'alg' + ' field missing.'
        return { validated: false, description }
    }
    if (alg.substring(0, 2) != 'RS') {
        description = 'Client assertion JWT header ' + 'alg' + ' field is different algorithm than RS.'
        return { validated: false, description }
    }
    if (alg.substring(2, 5) < 256) {
        description = 'Client assertion JWT header ' + 'alg' + ' field has lower value than RS256 (i.e. 128, 64 etc)'
        return { validated: false, description }
    }
    if (!typ) {
        description = 'Client assertion JWT header ' + 'typ' + ' field missing'
        return { validated: false, description }
    }
    if (typ != 'JWT') {
        description = 'Client assertion JWT header ' + 'typ' + ' field is other value than ' + 'jwt' + ''
        return { validated: false, description }
    }
    if (!x5c) {
        description = 'Client assertion JWT header contains no ' + 'x5c' + ' array'
        return { validated: false, description }
    }

    var iss = decodedClientAssertion.payload.iss
    var sub = decodedClientAssertion.payload.sub
    var aud = decodedClientAssertion.payload.aud
    var exp = decodedClientAssertion.payload.exp
    var iat = decodedClientAssertion.payload.iat
    var jti = decodedClientAssertion.payload.jti

    if (!iss) {
        description = 'Client assertion JWT payload ' + 'iss' + ' field missing'
        return { validated: false, description }
    }
    if (iss != client_id) {
        description = 'Client assertion JWT payload ' + 'iss' + ' field is different value from Client_id request parameter'
        return { validated: false, description }
    }
    if (!sub) {
        description = 'Client assertion JWT payload ' + 'sub' + ' field missing'
        return { validated: false, description }
    }
    if (iss != sub) {
        description = 'Client assertion JWT payload "sub" field is different value than ' + 'iss' + ' field'
        return { validated: false, description }
    }
    if (!aud) {
        description = 'Client assertion JWT payload ' + 'aud' + ' field missing'
        return { validated: false, description }
    }
    if (aud != eori_sp) {
        description = 'Client assertion JWT payload ' + 'aud' + ' field is different value than the server iSHARE client id'
        return { validated: false, description }
    }
    if (!jti) {
        description = 'Client assertion JWT payload ' + 'jti' + ' field missing'
        return { validated: false, description }
    }
    if (!exp) {
        description = 'Client assertion JWT payload ' + 'exp' + ' field missing'
        return { validated: false, description }
    }
    if (exp != iat + 30) {
        description = 'Client assertion JWT payload ' + 'exp' + ' field is different value than ' + 'iat' + ' field + 30 seconds'
        return { validated: false, description }
    }
    if (!iat) {
        description = 'Client assertion JWT payload "iat" field missing'
        return { validated: false, description }
    }
    // 5 is addeed here because of time sync issues
    if (iat > epoch + 5) {
        description = 'Client assertion JWT payload ' + 'iat' + 'field is after current time'
        return { validated: false, description }
    }
    if (!client_assertion.split('.').slice(2).join('.')) {
        return { validated: false, description: 'Client assertion JWT signature missing' }
    }

    var x5cStrigified = JSON.stringify(x5c)
    var result = x5cStrigified.substring(2, x5cStrigified.length - 2);
    var begin_cert = '-----BEGIN CERTIFICATE-----\n'
    var end_cert = '\n-----END CERTIFICATE-----'
    var pem_cert = begin_cert.concat(result, end_cert);

    try {
        var decoded = jwt.verify(client_assertion, pem_cert, {
            algorithms: 'RS256'
        });
        var derKey = forge.util.decode64(x5c[0]);
        var asnObj = forge.asn1.fromDer(derKey);
        var asn1Cert = forge.pki.certificateFromAsn1(asnObj);

        /*        logger.debug("Bit length: ", publicKey.n.bitLength());
                        if (publicKey.n.bitLength() < 2048) {
                            return { validated: false, description: 'invalid client' }
                        }*/
        try {
            // forge.pki.verifyCertificateChain(caStore, [asn1Cert]);
            return { validated: true, description: 'verified client_assertion' }
        } catch (e) {
            logger.debug('Failed to verify certificate (' + e.message || e + ')');
            return { validated: false, description: 'Failed to verify certificate' }
        }

    } catch (err) {
        logger.debug('ERR decoded client_assertion: ' + err)
        if (err == 'TokenExpiredError: jwt expired') {
            description = 'Client is not authorized by Simply Deliver: JWT is expired.'
        } else {
            description = 'Client assertion JWT header "x5c" contains invalid certificate'
        }
        return { validated: false, description }
    }
}

const createCapabilities = (eori_sp, base_url, privateKey, publicKey) => {

    var capabilities = {
        'party_id': eori_sp,
        "supported_versions": [{
            "version": "1.0",
            "supported_features": [{
                "public": [{
                        "id": v4(),
                        "feature": "capabilities",
                        "description": "Retrieves API capabilities",
                        "url": base_url + "/capabilities",
                    },
                    {
                        "id": v4(),
                        "feature": "access_token",
                        "description": "Obtains access token",
                        "url": base_url + "/api/inner/token"
                    },
                    {
                        "id": v4(),
                        "feature": "relay",
                        "description": "Relay a message to a data space via a remote proxy",
                        "url": base_url + "/api/inner/relay",
                        "token_endpoint": base_url + "/api/inner/token"
                    }
                ]
            }]
        }]
    }
    var iat = Math.floor(new Date() / 1000)
    var header = { "x5c": [pemToX5C(publicKey)] };
    var payload = {
        "iss": eori_sp,
        "sub": eori_sp,
        "jti": v4(),
        "iat": iat,
        "exp": iat + 30,
        "capabilities_info": capabilities
    };
    return jwt.sign(payload, privateKey, { algorithm: 'RS256', header: header, noTimestamp: false })

}

// the exports 
exports.createCapabilities = createCapabilities
exports.check_assertion = check_assertion
exports.verifyClientAccessToken = verifyClientAccessToken
exports.serviceAssertionToken = serviceAssertionToken
exports.clientAccessToken = clientAccessToken