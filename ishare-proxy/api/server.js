const express = require('express')
const path = require('path')
const cors = require('cors')
const fs = require('fs')
const jwt = require('jsonwebtoken')
const util = require('util')
const ishare = require('./ishare.js')
const logger = require('loglevel').getLogger("server")
const axios = require('axios')

// set Log level
const logLevel = process.env.LOG_LEVEL || "debug"
logger.setLevel(logLevel)

const port = process.env.SERVER_PORT || 8081;

// your information
// Base URL for the service
const base_url = process.env.BASE_URL || `http://localhost:${port}`
const certName = process.env.CERT_NAME || 'CERT'
const eori_sp = process.env.ISHARE_ID || "<insert default EORI>"
const scheme_url = process.env.SCHEME_URL || "https://scheme.isharetest.net"

// Import certificateUtils
const certificateUtils = require('./certificateUtils.js');
const { application } = require('express')


//Read private and public key 
const privateKey = fs.readFileSync(`./cert/${certName}.key`, 'utf8');
const publicKey = fs.readFileSync(`./cert/${certName}.crt`, 'utf8');

const app = express(),
    bodyParser = require("body-parser");
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(cors());
app.use(express.static(path.join(__dirname, '../html')));


app.post('/api/inner/token', async(req, res) => {
    var reqBody = await req.body
    var client_id = reqBody.client_id
    logger.debug(`token request received : ${new URLSearchParams(reqBody).toString()}`)
    logger.debug('checking client assertion...');
    const checkResult = ishare.check_assertion(reqBody, eori_sp)

    var contentLength = reqBody.length;
    if (!checkResult.validated) {
        res.status(400).json({
            status: "Not Active",
            message: checkResult.description
        });
        return;
    }
    logger.debug('client_assertion checked')
    logger.debug('checking iSHARE status...')
    const client_assertion_sp = ishare.serviceAssertionToken(privateKey, publicKey, 'EU.EORI.NL000000000', eori_sp)
    logger.debug('Client Assertion Token: \n' + util.inspect(client_assertion_sp))
    logger.debug(util.inspect(jwt.decode(client_assertion_sp, { complete: true })))
    var reqBody = new URLSearchParams({
        'grant_type': 'client_credentials',
        'scope': 'iSHARE',
        'client_assertion_type': 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
        'client_assertion': client_assertion_sp,
        'client_id': eori_sp
    }).toString();

    const tokenRequest = await axios.post(`${scheme_url}/connect/token`, reqBody, {
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    });
    var access_token = tokenRequest.data.access_token;

    logger.debug(`Access token: ${access_token}`)
    const party_token = await axios.get(`${scheme_url}/parties?eori=${client_id}`, {
        headers: {
            'Authorization': `Bearer ${access_token}`
        }
    });

    const decoded_party_token = jwt.decode(party_token.data.parties_token);
    logger.debug(`Parties repsonse: ${JSON.stringify(decoded_party_token)}`)
    if (decoded_party_token.parties_info.data[0].adherence.status == 'Active') {
        const access_token = ishare.clientAccessToken(eori_sp, client_id, privateKey);
        logger.debug('Access Token for client: ' + access_token)
        res.status(200).json({
            access_token: access_token,
            expires_in: 3600,
            token_type: 'Bearer'
        })
        return;
    } else {
        res.status(400).json({
            message: 'invalid client'
        });
        logger.info('status is not active')
        return;
    }
})

app.post('/api/inner/relay', async(req, res) => {
    logger.debug("received request : " + JSON.stringify(res.body));
    logger.debug("-------------------------------------------");
    // check access token
    if (!ishare.verifyClientAccessToken(req.headers.authorization.substring(7), privateKey, eori_sp)) {
        res.status(401).json({
            status: "token invalid"
        });
        return res
    }
    try {
        const tokenURI = `${req.body.remoteproxy}/api/security/token/userpass`

        var tokenParams = new URLSearchParams({
            'username': 'ishareproxy',
            'password': 'ishareproxy'
        }).toString();

        logger.debug(`requesting token at: ${tokenURI}`);
        logger.debug(`with options : ${tokenParams}`);
        logger.debug("-------------------------------------------");

        const tokenRequest = await axios.post(tokenURI, tokenParams, {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        });
        logger.debug("received code : " + tokenRequest.status);
        logger.debug("with data : " + tokenRequest.data);
        logger.debug("-------------------------------------------");
        const token = tokenRequest.data

        const resourceBody = {
            action: 'read',
            method: 'GET',
            params: {
                dataset: req.body.dataset,
                query: req.body.query,
                format: req.body.format
            },
            messageType: req.body.messageType,
            artifact: req.body.artifact,
            provider: req.body.provider

        }
        const resourceURI = `${req.body.remoteproxy}/api/proxy/resource`
        logger.debug("requesting data at : " + resourceURI);
        logger.debug("with body : " + JSON.stringify(resourceBody));
        logger.debug("and token : " + token)
        logger.debug("-------------------------------------------");
        const dataRequest = await axios.post(resourceURI, resourceBody, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        logger.debug("received code : " + dataRequest.status);
        logger.debug("with data : " + JSON.stringify(dataRequest.data));
        logger.debug("-------------------------------------------");
        res.set('Content-Type', dataRequest.headers['content-type'] || 'text/plain')
        res.send(dataRequest.data)
    } catch (error) {
        if (error.response) {
            res.status(error.response.status);
            res.send(error.response.data);
        } else {
            res.status(400);
            res.send();
            logger.info(error)
        }
    }
})

app.get('/api/inner/capabilities', async(req, res) => {
    try {
        res.status(200).json({
            capabilities_token: ishare.createCapabilities(eori_sp, base_url, privateKey, publicKey)
        })
    } catch (error) {
        logger.info(error)
        res.status(500);
        res.send();
    }
})


app.listen(port, () => {
    logger.info(`Server listening on the port::${port}`);
});