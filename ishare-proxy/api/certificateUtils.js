const fs    = require('fs');
const jwt   = require('jsonwebtoken');

const pemToX5C = (pemString) => {
    // Regex from https://github.com/digitalbazaar/forge lib/pem.js
    const pemRegex = /\s*-----BEGIN ([A-Z0-9- ]+)-----\r?\n?([\x21-\x7e\s]+?(?:\r?\n\r?\n))?([:A-Za-z0-9+\/=\s]+?)-----END \1-----/g;
    var x5c = []
    var match;
    while (true) {
        match = pemRegex.exec(pemString);
        if (!match) {
            break;
        }
        x5c.push(match[3].replace(/\s/g, ''));
    }
    return x5c;
}

const pemFileToX5C = (fileName) => {
    return pemToX5C(fs.readFileSync(fileName, 'utf8').toString())
}

const getCertificatePublicKeyBitLength = (pemFile) => {
    const certificate = forge.pki.certificateFromPem(pemFile);
    return certificate.publicKey.n.bitLength();
}

exports.pemToX5C = pemToX5C
exports.pemFileToX5C = pemFileToX5C
exports.getCertificatePublicKeyBitLength = getCertificatePublicKeyBitLength