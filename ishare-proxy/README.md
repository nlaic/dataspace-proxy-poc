# SAML SPARQL UI
This repository contains a Vue application that acts as the front-end for the Sparql query consumer.

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

## Project setup
```
cd api
npm install
```

### Compiles and hot-reloads for development
```
cd api
node server.js
```
### API Definition

Send a message to a remote provider through a remote proxy : 

    Endpoint : <proxy url>:<proxy port>/api/proxy/relay 
    body : {    "provider": "http://<remote provider url>:<remote provider port>",
                "dataset": "<dataset as known in fuseki>",
                "format": "JSON | XML",
                "query": "SELECT ?subject ?predicate ?object WHERE { ?subject ?predicate ?object } LIMIT 25" ,
                "messageType": "ArtifactRequestMessage|QueryMessage|InvokeOperationMessage",
                "artifact": "<artifact as defined by the provider>" , (only required for messageType: ArtifactRequestMessage)
                "proxy": "http://<remote proxy url>:<remote proxy port>" }

For this Poc we use the ArtifactRequestMessage with artifact : http://sparql

### Docker Image usage

```
docker run -it --rm -p 8080:80 -e LOG_LEVEL=info BASE_URL=localhost  registry.ids.smart-connected.nl/ishare-proxy
```

## Configuration
The Worker GUI needs the following environment variables set to work properly

| Key         | Description                                                                                                                        |
|-------------|------------------------------------------------------------------------------------------------------------------------------------|
| LOG_LEVEL   | info / debug / warn / error <br/>log above (from left to right) the level will not be shown in the console output<br/>default: info |
| BASE_URL    | Base url of the proxy service                                                                                                      |
| CERT_NAME   | Name of the file where the iSHARE certificate is located in                                                                        |
| ISHARE_ID   | The iSHARE EORI                                                                                                                    |
| SCHEME_URL  | URI of the iSHARE scheme/satellite                                                                                                 |
| CA_FOLDER   | Folder where the iSHARE CA certificates are located                                                                                |  
