#!/bin/bash

. certificates.env

cat <<EOF >> $1
- uuid: $(cat /proc/sys/kernel/random/uuid)
  idsid: '$2'
  name: '$4'
  participant: '$3'
  contact: '${DAPS_CONTACT}'
  email: '${EMAILADDRESS}'
  claim:
    componentCertification:
      active: true
      lastActive: 2021-01-01T00:00:00+00:00
      securityProfile: Trust
    participantCertification:
      active: true
      lastActive: 2021-01-01T00:00:00+00:00
      level: Central
  # /identity-provider-scripts/certificates/connectors/$2/connector.pem
  certificate: |-
$(sed -e 's/^/    /' certificates/connectors/$2/connector.pem)
EOF

