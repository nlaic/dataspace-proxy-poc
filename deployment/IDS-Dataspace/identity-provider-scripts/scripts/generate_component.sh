#!/bin/bash
#
# Copyright (C) 2013-2015 with regard to distribution / exploitation:
# Fraunhofer-Gesellschaft zur Förderung der angewandten Forschung e.V.
# 

cleanup(){
echo "Cleanup unnecessary files"
[[ -f ${CONNECTOR_CSR} ]] && rm ${CONNECTOR_CSR}
[[ -f ${CONNECTOR_CONFIG} ]] && rm ${CONNECTOR_CONFIG}
[[ -f ${INDEX_FILE} ]] && rm ${INDEX_FILE}
[[ -f ${SERIAL_FILE} ]] && rm ${SERIAL_FILE}
[[ -f ${INDEX_FILE}.attr ]] && rm ${INDEX_FILE}.attr
[[ -f ${INDEX_FILE}.old ]] && rm ${INDEX_FILE}.old
[[ -f ${SERIAL_FILE}.old ]] && rm ${SERIAL_FILE}.old
[[ -f 01.pem ]] && rm 01.pem
}

error_check(){
if [ "$1" != "0" ]; then
  echo "Error: $2"
  cleanup
  popd
  exit 1
fi
}

assert_file_exists(){
if [ ! -f $1 ]; then 
  echo "Error: Missing file $1"
  popd
  exit 1
fi
}

assert_file_not_exists(){
if [ -f $1 ]; then 
  echo "Error: File $1 exists. Precautional exit"
  popd
  exit 1
fi
}

. certificates.env

# signing files
INDEX_FILE="../certificates/index.txt"
SERIAL_FILE="../certificates/serial.txt"

#CA files
SUBCA_CONFIG="../conf/devicesubca.cnf"
SUBCA_CERT="../certificates/devicesubca.cert"
SUBCA_KEY="../certificates/devicesubca.key"
CACHAIN_CERT="../certificates/cachain.cert"

# if [ $# -ge 1 ] ; then
#   echo "No arguments supplied, which is good"
# else
#   echo "Invalid number of arguments: $# (expected 1)"
#   echo "Usage: $0 connector-name"
#   exit 1
# fi


#CONNECTOR certificate files
CONNECTOR_NAME=$1
CONNECTOR_CONFIG="${CONNECTOR_NAME}.cnf"
CONNECTOR_CONFIG_TEMPLATE="../conf/connector.cnf.template"
CONNECTOR_CSR="../certificates/connectors/$1/connector.csr"
CONNECTOR_CERT="../certificates/connectors/$1/connector.cert"
CONNECTOR_PEM="../certificates/connectors/$1/connector.pem"
CONNECTOR_KEY="../certificates/connectors/$1/connector.key"

pushd $(dirname $0)
echo "Check if directory is clean"
assert_file_not_exists ${INDEX_FILE}
assert_file_not_exists ${SERIAL_FILE}
assert_file_not_exists ${CONNECTOR_CSR}
assert_file_not_exists ${CONNECTOR_KEY}
assert_file_not_exists ${CONNECTOR_CERT}
assert_file_not_exists ${CONNECTOR_PEM}
assert_file_not_exists ${CONNECTOR_CONFIG}

echo "Trying to find required files"
assert_file_exists ${SUBCA_KEY}
assert_file_exists ${SUBCA_CERT}
assert_file_exists ${SUBCA_CONFIG}
assert_file_exists ${CACHAIN_CERT}
assert_file_exists ${CONNECTOR_CONFIG_TEMPLATE}
echo "Successfully found requrired files"

echo "Making directory"

mkdir ../certificates/connectors/${CONNECTOR_NAME}

echo "Create connector config"
# copy template and set random values to uuid&serial
cp ${CONNECTOR_CONFIG_TEMPLATE} ${CONNECTOR_CONFIG}

# create uuid and serial
UUID=$(cat /proc/sys/kernel/random/uuid)
sed -i "s/%%CONNECTOR_NAME%%/${CONNECTOR_NAME}/g" ${CONNECTOR_CONFIG}
sed -i "s/%%ADDITIONAL_ALT_NAMES%%/$2/g" ${CONNECTOR_CONFIG}
sed -i "s/%%COUNTRY%%/${COUNTRY}/g" ${CONNECTOR_CONFIG}
sed -i "s/%%STATE%%/${STATE}/g" ${CONNECTOR_CONFIG}
sed -i "s/%%LOCALITY%%/${LOCALITY}/g" ${CONNECTOR_CONFIG}
sed -i "s/%%ORGANIZATION%%/${ORGANIZATION}/g" ${CONNECTOR_CONFIG}
sed -i "s/%%ORGANIZATIONAL_UNIT%%/${ORGANIZATIONAL_UNIT}/g" ${CONNECTOR_CONFIG}
sed -i "s/%%DNS_POSTFIX%%/${DNS_POSTFIX}/g" ${CONNECTOR_CONFIG}
sed -i "s/%%EMAILADDRESS%%/${EMAILADDRESS}/g" ${CONNECTOR_CONFIG}

echo "Create dummy connector CSR"
openssl req -batch -config ${CONNECTOR_CONFIG} -newkey rsa:2048 -sha256 -nodes -out ${CONNECTOR_CSR} -outform PEM
error_check $? "Failed to create dummy connector CSR"

echo "Sign dummy connector CSR with sub CA certificate"
touch ${INDEX_FILE}
touch ${SERIAL_FILE}
echo '01' > ${SERIAL_FILE}
openssl ca -batch -config ${SUBCA_CONFIG} -policy signing_policy -extensions signing_req -out ${CONNECTOR_CERT} -infiles ${CONNECTOR_CSR}
error_check $? "Failed to sign connector CSR with CA certificate"

echo "Verify newly created dummy connector certificate"
openssl verify -CAfile ${CACHAIN_CERT} ${CONNECTOR_CERT}
error_check $? "Failed to verify newly signed dummy certificate"

sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p
/-END CERTIFICATE-/q' ${CONNECTOR_CERT} > ${CONNECTOR_PEM}

echo "Dummy certificates successfully created"
cleanup
popd
exit 0
