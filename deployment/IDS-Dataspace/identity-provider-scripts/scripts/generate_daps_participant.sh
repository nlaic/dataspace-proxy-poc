#!/bin/bash

. certificates.env

cat <<EOF >> $1
- id: '$2'
  contact: '${DAPS_CONTACT}'
  email: '${EMAILADDRESS}'
  certification:
    active: true
    lastActive: 2021-01-01T00:00:00+00:00
    level: Central
  # /identity-provider-scripts/certificates/participants/$2/participant.pem
  certificate: |-
$(sed -e 's/^/    /' certificates/participants/$2/participant.pem)
EOF

