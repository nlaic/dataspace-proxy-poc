#!/bin/bash
#

error_check(){
if [ "$1" != "0" ]; then
  echo "Error: $2"
  popd
  exit 1
fi
}

assert_file_exists(){
if [ ! -f $1 ]; then 
  echo "Error: Missing file $1"
  popd
  exit 1
fi
}

assert_file_not_exists(){
if [ -f $1 ]; then 
  echo "Error: File $1 exists. Precautional exit"
  popd
  exit 1
fi
}


#root CA cert file
ROOTCA_CERT="../certificates/rootca.cert"
#device sub CA cert file
DEVICESUBCA_CERT="../certificates/devicesubca.cert"
#participant sub CA cert file
PARTICIPANTSUBCA_CERT="../certificates/participantsubca.cert"

#ca chain file
CACHAIN_CERT="../certificates/cachain.cert"

if [ $# -eq 0 ] ; then
  echo "No arguments supplied, which is good"
else
  echo "Invalid number of arguments: $# (expected 0)"
  echo "Usage: $0"
  exit 1
fi

pushd $(dirname $0)
echo "Check if directory is clean"
assert_file_not_exists ${CACHAIN_CERT}

echo "Trying to find required files"
assert_file_exists ${ROOTCA_CERT}
assert_file_exists ${DEVICESUBCA_CERT}
assert_file_exists ${PARTICIPANTSUBCA_CERT}
echo "Successfully found requrired files"

echo "Concatenate root CA to sub CA"
sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p
/-END CERTIFICATE-/q' ${ROOTCA_CERT} > ${CACHAIN_CERT}
echo "" >> ${CACHAIN_CERT}
sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p
/-END CERTIFICATE-/q' ${DEVICESUBCA_CERT} >> ${CACHAIN_CERT}
echo "" >> ${CACHAIN_CERT}
sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p
/-END CERTIFICATE-/q' ${PARTICIPANTSUBCA_CERT} >> ${CACHAIN_CERT}

popd
exit 0
