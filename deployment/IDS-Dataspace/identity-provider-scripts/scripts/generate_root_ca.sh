#!/bin/bash

error_check(){
if [ "$1" != "0" ]; then
  echo "Error: $2"
  cleanup
  popd
  exit 1
fi
}

assert_file_exists(){
if [ ! -f $1 ]; then 
  echo "Error: Missing file $1"
  popd
  exit 1
fi
}

assert_file_not_exists(){
if [ -f $1 ]; then 
  echo "Error: File $1 exists. Precautional exit"
  popd
  exit 1
fi
}

. certificates.env

#root CA config template
ROOTCA_CONFIG_TEMPLATE="../conf/rootca.cnf.template"
#root CA config file
ROOTCA_CONFIG="../conf/rootca.cnf"
#root CA cert file
ROOTCA_CERT="../certificates/rootca.cert"
#root CA key file
ROOTCA_KEY="../certificates/rootca.key"

if [ $# -eq 0 ] ; then
  echo "No arguments supplied, which is good"
else
  echo "Invalid number of arguments: $# (expected 0)"
  echo "Usage: $0"
  exit 1
fi

pushd $(dirname $0)
echo "Check if directory is clean"
assert_file_not_exists ${ROOTCA_CERT}
assert_file_not_exists ${ROOTCA_KEY}

echo "Trying to find required files"
assert_file_exists ${ROOTCA_CONFIG_TEMPLATE}
echo "Successfully found requrired files"

echo "Create Root CA config"
cp ${ROOTCA_CONFIG_TEMPLATE} ${ROOTCA_CONFIG}

sed -i "s/%%COUNTRY%%/${COUNTRY}/g" ${ROOTCA_CONFIG}
sed -i "s/%%ORGANIZATION%%/${ORGANIZATION}/g" ${ROOTCA_CONFIG}
sed -i "s/%%COMMON_NAME%%/${COMMON_NAME}/g" ${ROOTCA_CONFIG}

########## ROOT CA CERT ########## 
# -nodes option omits passphrase
echo "Create self-signed root CA certificate"
openssl req -batch -x509 -config ${ROOTCA_CONFIG} -days 7300 -newkey rsa:2048 -sha1 -nodes -out ${ROOTCA_CERT} -outform PEM
error_check $? "Failed to create self signed root CA certificate"
popd
exit 0
