#!/bin/bash
#
# Copyright (C) 2013-2015 with regard to distribution / exploitation:
# Fraunhofer-Gesellschaft zur Förderung der angewandten Forschung e.V.
# 

cleanup(){
echo "Cleanup unnecessary files"
[[ -f ${PARTICIPANT_CSR} ]] && rm ${PARTICIPANT_CSR}
[[ -f ${PARTICIPANT_CONFIG} ]] && rm ${PARTICIPANT_CONFIG}
[[ -f ${INDEX_FILE} ]] && rm ${INDEX_FILE}
[[ -f ${SERIAL_FILE} ]] && rm ${SERIAL_FILE}
[[ -f ${INDEX_FILE}.attr ]] && rm ${INDEX_FILE}.attr
[[ -f ${INDEX_FILE}.old ]] && rm ${INDEX_FILE}.old
[[ -f ${SERIAL_FILE}.old ]] && rm ${SERIAL_FILE}.old
[[ -f 01.pem ]] && rm 01.pem
}
cleanupCsrOnly(){
echo "Cleanup unnecessary files"
[[ -f ${PARTICIPANT_CONFIG} ]] && rm ${PARTICIPANT_CONFIG}
[[ -f ${INDEX_FILE} ]] && rm ${INDEX_FILE}
[[ -f ${SERIAL_FILE} ]] && rm ${SERIAL_FILE}
[[ -f ${INDEX_FILE}.attr ]] && rm ${INDEX_FILE}.attr
[[ -f ${INDEX_FILE}.old ]] && rm ${INDEX_FILE}.old
[[ -f ${SERIAL_FILE}.old ]] && rm ${SERIAL_FILE}.old
[[ -f 01.pem ]] && rm 01.pem
}

error_check(){
if [ "$1" != "0" ]; then
  echo "Error: $2"
  cleanup
  popd
  exit 1
fi
}

assert_file_exists(){
if [ ! -f $1 ]; then 
  echo "Error: Missing file $1"
  popd
  exit 1
fi
}

assert_file_not_exists(){
if [ -f $1 ]; then 
  echo "Error: File $1 exists. Precautional exit"
  popd
  exit 1
fi
}

. certificates.env

# signing files
INDEX_FILE="../certificates/index.txt"
SERIAL_FILE="../certificates/serial.txt"

#CA files
SUBCA_CONFIG="../conf/participantsubca.cnf"
SUBCA_CERT="../certificates/participantsubca.cert"
SUBCA_KEY="../certificates/participantsubca.key"
CACHAIN_CERT="../certificates/cachain.cert"

# if [ $# -ge 1 ] ; then
#   echo "No arguments supplied, which is good"
# else
#   echo "Invalid number of arguments: $# (expected 1)"
#   echo "Usage: $0 connector-name"
#   exit 1
# fi


#PARTICIPANT certificate files
PARTICIPANT_NAME=$1
PARTICIPANT_LINUX_FRIENDLY_NAME=${PARTICIPANT_NAME// /_}
PARTICIPANT_CONFIG="${PARTICIPANT_LINUX_FRIENDLY_NAME}.cnf"
PARTICIPANT_CONFIG_TEMPLATE="../conf/participant.cnf.template"
PARTICIPANT_CSR="../certificates/participants/$PARTICIPANT_LINUX_FRIENDLY_NAME/participant.csr"
PARTICIPANT_CERT="../certificates/participants/$PARTICIPANT_LINUX_FRIENDLY_NAME/participant.cert"
PARTICIPANT_PEM="../certificates/participants/$PARTICIPANT_LINUX_FRIENDLY_NAME/participant.pem"
PARTICIPANT_KEY="../certificates/participants/$PARTICIPANT_LINUX_FRIENDLY_NAME/participant.key"

pushd $(dirname $0)
echo "Check if directory is clean"
assert_file_not_exists ${INDEX_FILE}
assert_file_not_exists ${SERIAL_FILE}
assert_file_not_exists ${PARTICIPANT_CSR}
assert_file_not_exists ${PARTICIPANT_KEY}
assert_file_not_exists ${PARTICIPANT_CERT}
assert_file_not_exists ${PARTICIPANT_PEM}
assert_file_not_exists ${PARTICIPANT_CONFIG}

echo "Trying to find required files"
assert_file_exists ${SUBCA_KEY}
assert_file_exists ${SUBCA_CERT}
assert_file_exists ${SUBCA_CONFIG}
assert_file_exists ${CACHAIN_CERT}
assert_file_exists ${PARTICIPANT_CONFIG_TEMPLATE}
echo "Successfully found requrired files"

echo "Making directory"

mkdir ../certificates/participants/${PARTICIPANT_LINUX_FRIENDLY_NAME}

echo "Create participant config"
# copy template and set random values to uuid&serial
cp ${PARTICIPANT_CONFIG_TEMPLATE} ${PARTICIPANT_CONFIG}

# create uuid and serial
UUID=$(cat /proc/sys/kernel/random/uuid)
sed -i "s/%%PARTICIPANT_NAME%%/${PARTICIPANT_NAME}/g" ${PARTICIPANT_CONFIG}
sed -i "s/%%PARTICIPANT_LINUX_FRIENDLY_NAME%%/${PARTICIPANT_LINUX_FRIENDLY_NAME}/g" ${PARTICIPANT_CONFIG}
sed -i "s/%%ADDITIONAL_ALT_NAMES%%/$2/g" ${PARTICIPANT_CONFIG}
sed -i "s/%%COUNTRY%%/${COUNTRY}/g" ${PARTICIPANT_CONFIG}
sed -i "s/%%STATE%%/${STATE}/g" ${PARTICIPANT_CONFIG}
sed -i "s/%%LOCALITY%%/${LOCALITY}/g" ${PARTICIPANT_CONFIG}
sed -i "s/%%ORGANIZATION%%/${ORGANIZATION}/g" ${PARTICIPANT_CONFIG}
sed -i "s/%%ORGANIZATIONAL_UNIT%%/${ORGANIZATIONAL_UNIT}/g" ${PARTICIPANT_CONFIG}
sed -i "s/%%DNS_POSTFIX%%/${DNS_POSTFIX}/g" ${PARTICIPANT_CONFIG}
sed -i "s/%%EMAILADDRESS%%/${EMAILADDRESS}/g" ${PARTICIPANT_CONFIG}

echo "Create dummy participant CSR"
openssl req -batch -config ${PARTICIPANT_CONFIG} -newkey rsa:2048 -sha256 -nodes -out ${PARTICIPANT_CSR} -outform PEM
error_check $? "Failed to create dummy participant CSR"

if [ $2 == "csrOnly" ]; then
  echo "Certificate Signing Request successfully created"
  cleanupCsrOnly
  popd
  exit 0
fi

echo "Sign dummy participant CSR with sub CA certificate"
touch ${INDEX_FILE}
touch ${SERIAL_FILE}
echo '01' > ${SERIAL_FILE}
openssl ca -batch -config ${SUBCA_CONFIG} -policy signing_policy -extensions signing_req -out ${PARTICIPANT_CERT} -infiles ${PARTICIPANT_CSR}
error_check $? "Failed to sign participant CSR with CA certificate"

echo "Verify newly created dummy participant certificate"
openssl verify -CAfile ${CACHAIN_CERT} ${PARTICIPANT_CERT}
error_check $? "Failed to verify newly signed dummy certificate"

sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p
/-END CERTIFICATE-/q' ${PARTICIPANT_CERT} > ${PARTICIPANT_PEM}

echo "Dummy certificates successfully created"
cleanup
popd
exit 0
