#!/bin/bash
#

cleanup(){
echo "Cleanup unnecessary files"
[[ -f ${SUBCA_CSR} ]] && rm ${SUBCA_CSR}
[[ -f ${INDEX_FILE} ]] && rm ${INDEX_FILE}
[[ -f ${SERIAL_FILE} ]] && rm ${SERIAL_FILE}
[[ -f ${INDEX_FILE}.attr ]] && rm ${INDEX_FILE}.attr
[[ -f ${INDEX_FILE}.old ]] && rm ${INDEX_FILE}.old
[[ -f ${SERIAL_FILE}.old ]] && rm ${SERIAL_FILE}.old
[[ -f 01.pem ]] && rm 01.pem
}

error_check(){
if [ "$1" != "0" ]; then
  echo "Error: $2"
  cleanup
  popd
  exit 1
fi
}

assert_file_exists(){
if [ ! -f $1 ]; then 
  echo "Error: Missing file $1"
  popd
  exit 1
fi
}

assert_file_not_exists(){
if [ -f $1 ]; then 
  echo "Error: File $1 exists. Precautional exit"
  popd
  exit 1
fi
}

. certificates.env

#root CA sign index file
INDEX_FILE="../certificates/index.txt"
#root CA sign serial file
SERIAL_FILE="../certificates/serial.txt"
#root CA cert file
ROOTCA_CERT="../certificates/rootca.cert"
#root CA key file
ROOTCA_KEY="../certificates/rootca.key"
#root CA config file
ROOTCA_CONFIG="../conf/rootca.cnf"

#sub CA config template
SUBCA_CONFIG_TEMPLATE="../conf/participantsubca.cnf.template"
#sub CA config file
SUBCA_CONFIG="../conf/participantsubca.cnf"
#sub CA csr file
SUBCA_CSR="../certificates/participantsubca.csr"
#sub CA cert file
SUBCA_CERT="../certificates/participantsubca.cert"
#sub CA key file
SUBCA_KEY="../certificates/participantsubca.key"

if [ $# -eq 0 ] ; then
  echo "No arguments supplied, which is good"
else
  echo "Invalid number of arguments: $# (expected 0)"
  echo "Usage: $0"
  exit 1
fi

pushd $(dirname $0)
echo "Check if directory is clean"
assert_file_not_exists ${INDEX_FILE}
assert_file_not_exists ${SERIAL_FILE}
assert_file_not_exists ${SUBCA_CERT}
assert_file_not_exists ${SUBCA_CSR}
assert_file_not_exists ${SUBCA_KEY}

echo "Trying to find required files"
assert_file_exists ${ROOTCA_CERT}
assert_file_exists ${ROOTCA_KEY}
assert_file_exists ${ROOTCA_CONFIG}
assert_file_exists ${SUBCA_CONFIG_TEMPLATE}
echo "Successfully found requrired files"

echo "Create Root CA config"
cp ${SUBCA_CONFIG_TEMPLATE} ${SUBCA_CONFIG}

sed -i "s/%%COUNTRY%%/${COUNTRY}/g" ${SUBCA_CONFIG}
sed -i "s/%%ORGANIZATION%%/${ORGANIZATION}/g" ${SUBCA_CONFIG}
sed -i "s/%%COMMON_NAME%%/${COMMON_NAME}/g" ${SUBCA_CONFIG}

# -nodes option omits passphrase
echo "Create sub CA CSR"
openssl req -batch -config ${SUBCA_CONFIG} -newkey rsa:4096 -sha1 -nodes -out ${SUBCA_CSR} -outform PEM
error_check $? "Failed to create sub CA CSR"

echo "Sign sub CA CSR with root CA"
touch ${INDEX_FILE}
touch ${SERIAL_FILE}
echo '01' > ${SERIAL_FILE}
openssl ca -batch -config ${ROOTCA_CONFIG} -policy signing_policy -extensions signing_req_CA -out ${SUBCA_CERT} -infiles ${SUBCA_CSR}
error_check $? "Failed to sign sub CA CSR with root CA certificate"

echo "Verify newly created sub CA certificate"
openssl verify -CAfile ${ROOTCA_CERT} ${SUBCA_CERT}
error_check $? "Failed to verify newly signed sub CA certificate"

echo "Cleanup temp files"
cleanup

popd
exit 0
