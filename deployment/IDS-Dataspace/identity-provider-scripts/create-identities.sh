#!/bin/bash

rm -rf certificates
mkdir -p certificates/connectors
mkdir -p certificates/participants

. ./certificates.env

# Generate CA Certificates
# Create ROOT Certificate Authority
COMMON_NAME="$ROOT_CA_CN" scripts/generate_root_ca.sh
# Create SUB Certificate Authority, used for signing connector certificates
COMMON_NAME="$DEVICE_CA_CN" scripts/generate_device_sub_ca.sh
# Create SUB Certificate Authority, used for signing participant certificates
COMMON_NAME="$PARTICIPANT_CA_CN" scripts/generate_participant_sub_ca.sh
# Concatenate both CAs to form CA chain
scripts/generate_ca_chain.sh

# Create certificates for initial Participants
# scripts/generate_participant.sh IDS_PARTICIPANT_ID
scripts/generate_participant.sh urn:ids:tno:participants:TNO
scripts/generate_participant.sh urn:ids:tno:participants:HOSPITAL
scripts/generate_participant.sh urn:ids:tno:participants:CONSUMER


# Create certificates for DAPS, Broker, and connectors
# scripts/generate_component.sh IDS_CONNECTOR_ID
scripts/generate_component.sh urn:ids:tno:DAPS
scripts/generate_component.sh urn:ids:tno:connector:HOSPITAL
scripts/generate_component.sh urn:ids:tno:connector:CONSUMER
scripts/generate_component.sh urn:ids:tno:connector:BROKER
scripts/generate_component.sh urn:ids:tno:connector:PROXY

scripts/generate_daps_participant.sh certificates/daps_participants.yaml urn:ids:tno:participants:TNO
scripts/generate_daps_participant.sh certificates/daps_participants.yaml urn:ids:tno:participants:CONSUMER
scripts/generate_daps_participant.sh certificates/daps_participants.yaml urn:ids:tno:participants:HOSPITAL

scripts/generate_daps_component.sh certificates/daps_components.yaml urn:ids:tno:connectors:BROKER urn:ids:tno:participants:TNO "Metadata Broker"
scripts/generate_daps_component.sh certificates/daps_components.yaml urn:ids:tno:connectors:CONSUMER urn:ids:tno:participants:CONSUMER "Metadata Consumer"
scripts/generate_daps_component.sh certificates/daps_components.yaml urn:ids:tno:connectors:HOSPITAL urn:ids:tno:participants:HOSPITAL "Metadata Hospital"
scripts/generate_daps_component.sh certificates/daps_components.yaml urn:ids:tno:connectors:PROXY urn:ids:tno:participants:TNO "Metadata Proxy"