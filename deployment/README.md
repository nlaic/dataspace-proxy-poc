# NL AIC DEMO Helm Charts

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

(Documentation generated using [helm-docs](https://github.com/norwoodj/helm-docs))

Umbrella chart for the NL AIC demo

## Deployment content
This umbrella chart deploys the following components using Helm charts:
- IDS broker
- IDS daps
- IDS Hospital 
- IDS Proxy 
- MongoDb instance
- InfluxDB instance 

- iSHARE Consumer
- iSHARE Proxy

- Keycloak 


## Install commands

### Create  certificates for IDS deployment

To create self-signed certificates that are used in the IDS deployment

Before creating the certifates the certificates.env should be adjusted to fit the preferred deployment


```
cd IDS-Dataspace/identity-provider-scripts
./create-identities.sh 
```

This will create certificates for the DAPS and connectors that are configured

#### Install Keycloak 

```
cd keycloak
helm repo add codecentric https://codecentric.github.io/helm-charts
helm install -n <namespace> keycloak bitnami/keycloak
```
Create a user for the iSHARE proxy:
- Click Users (left-hand menu)
- Click Add user (top-right corner of table)
- Fill in the form with the following values:
    - Username: ishareproxy
    - first name: ishare
    - Last name: proxy
    - Click Save
    - Click Credentials (top of the page)
    - Fill in the Set Password form with "ishareproxy"
    - Click OFF next to Temporary
    
Open the Keycloak Account Console https://(<keycloak-url>:<port>/auth/realms/HarmonizationSpace/account/#) do the following steps for both created users and check the accounts:
- login with ishareproxy/ishareproxy

Create a client (using the EU.EORI.NL123456789 as id) according to the steps in the manual(https://www.keycloak.org/docs/latest/authorization_services/  chapter: Enabling Authorization Services) : 
In the credential tab under the Client, generate a new secret and set this secret in the IDS-Dataspace/values.idsproxy.yaml at clientSecret.

Create a Client Scope named with the name: relay
In the Clients / EU.EORI.NL123456789 under "Client Scopes" add the newly create Client Scope "relay"to the assigned optional Client Scopes.

Keycloak should now be ready to handle the requests in the PoC 

#### Install the IDS Data space connectors
Make sure to install the Helm chart using:
```
cd IDS-Dataspace
helm dependency update  
helm upgrade --install <deplaoyment name> --create-namespace -n  <namespace> . -f values.idshospital.yaml  -f values.idsproxy.yaml  -f values.idsdaps.yaml  -f values.idsbroker.yaml 
```
to install the connectors for the IDS Hospital  The IDS Proxy , The IDS DAPS and the IDS Broker demo.

The yaml files contain certificates created by the scripts in the IDS-Datapace/identity-provider-scripts 

#### Install the iSHARE Data space connectors
Make sure to install the Helm chart using:
```
cd iSHARE-Dataspace
helm dependency update  
helm upgrade --install <deployment name> --create-namespace -n <namespace> . -f values.ishareconsumer.yaml  -f values.ishareproxy.
yaml
```
to install the connectors for the iSHARE sparql consumer and the iSHARE proxy demo.

## Configuration values
Most of the values present in the `values*.yaml` files are configured so that all services are connected.
The following values are important to change:
- **mongoDbPassword**: password used to connect to the mongodb instance
- **host**: (in each of the `values.*.yaml`) URL to reach a specific connector.

